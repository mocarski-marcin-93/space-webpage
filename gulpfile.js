var gulp = require('gulp');

gulp.task('default', function() {
  //
});

// SASS
var sass = require('gulp-sass');
gulp.task('sass', function() {
  return gulp.src('src/_scss/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('src/_css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

var watch = require('gulp-watch');
gulp.task('watch', ['browserSync', 'sass'] ,function(){
  gulp.watch('src/_scss/*.scss', ['sass']);
  // Other watchers
});

var browserSync = require('browser-sync').create();
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'src'
    },
  })
});
